---
id: art
title: Kącik rysowniczy
sidebar_label: Wystrój i kącik rysowniczy
---

### Wystrój
Koordynatorem jest Marime. Jej przypada zebranie drużyny oraz (samodzielne lub cudzymi łapkami) przygotowanie grafik na konwent, plakatów, wizytówek, wystrój stoiska, czyli zorganizowanie materiałów na ścianki i rozłożenie wszystkiego w czwartek / piątek. Ja pomogę jak tylko będę mogła, będę podrzucać pomysły i zadania, ale potrzebuję kogoś z inicjatywą, kto weźmie ode mnie propozycję czy zalążek i pchnie to dalej.    
Roboty od groma, jedna osoba tego nie zrobi, ale jedna osoba powinna chcieć zarządzać zespołem. Jeśli są osoby, które lubią zajmować się takimi sprawami, dajcie znać w formularzu. Jeśli ktoś nie ma zmysłu artystycznego, ale jest wysoki, może pomóc i np. umocować materiał na ściankach.  

### Kącik rysowniczy
Jak w poprzednich latach, jedną ławkę przeznaczymy na kącik rysowniczy. Będą tam blok rysunkowy, kolorowanki oraz przybory do rysowania (kredki, flamastry).  
Zadanie polega na zorganizowaniu powyższych przedmiotów i dostarczeniu je na stoisko w piątek rano. Jeśli możecie coś przynieść, wpiszcie to w formularz.

### Galeria
Na ściankach naszego stoiska będziemy, jak zwykle, przyczepiać rysunki wszystkich, którzy w ciągu Pyrkonu stworzą coś na stoisku NK.  

:::important
@Ilustrator NK @Animator NK  
Z tego, co wiem, obrazy/animacje do projektów NK tworzycie w wysokich rozdzielczościach. Przemyślcie czy nie chcecie wydrukować na Pyrkon kolażu narysowanych do projektów postaci  lub screenów 1-2 kadrów z animacji. Śmiało możecie na taki wydruk dać też swój nick (używany w NK) i jakiś namiar na siebie, żeby się poreklamować.
:::important
