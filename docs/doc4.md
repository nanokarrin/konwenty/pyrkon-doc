---
id: wd
title: Live Dubbing
sidebar_label: Live Dubbing
---

### Nagrywamy

Tutaj workflow dzieli się na trzy części: punkt info / rozmowy; reżyserię; nagrywanie.

##### Punkt info
Nagabywacze subtelnie wyłapują w tłumie tych, którzy wyglądają na zaciekawionych. Tłumaczą, co to NK, co to za stoisko, co robimy, jakie są możliwości. Poza tym pełnią ważną funkcję zawiadywania chętnymi do nagrań. Nagabywacz odsyła chętnego do listy kolejkowej (powracamy do starej dobrej kartki! rok temu nie było neta) i prosi o wpisanie się lub sam wpisuje konwentowicza DRUKOWANYMI LITERAMI.   
Zasady kolejkowe są proste:  
* jedna osoba może nagrać się pod jedną postać w scence
* lub może nagrać jedną piosenkę (przy czym można mieć max. 2-3 podejścia do piosenki, bo to mocno blokuje stanowisko).  

Dostaniemy dwa balony z helem, by dać je osobom, które czekają w kolejce. Balon z numerem 1 ma osoba, która wchodzi zaraz-już. W momencie siadania do kompa balon 1 idzie do osoby z balonem 2, a balon 2 idzie do członka NK, który ogarnia kolejkę. Dzięki temu zawsze mamy dwie osoby, które są w kolejce, zawsze widać, kto to jest, a ta osoba nie musi siedzieć tuż obok stanowiska, może stać, rysować, etc.  
Poza tym kolejka idzie jak dawniej, od najmniejszego numerka obecnego. Uwaga - nie zabieramy balonu temu, kto już go dostał.  
Pamiętajmy o wykreślaniu z listy!  
NIE prosimy o nr telefonów i NIE wysyłamy SMS-ów z wiadomością “jesteś drugi w kolejce”. Ten system sprawdzał się na małych konwentach, gdzie ludzie powiadomieni pojawiali się w minutę w naszej salce. Na Pyrkonie nie przychodzili nawet 15 minut, a nie ma co dla nich blokować stanowiska. Muszą czekać, trudno. Dodatkowo jeśli obiecacie komuś SMS-a lub telefon, a skończy się wasz dyżur, to zostajecie z tym obowiązkiem, nie przechodzi on na kolejnych dyżurujących - a zakładam, że wolelibyście po prostu sobie pójść.  
Jeśli kolejki nie ma i wolontariusz się upewni (czyli spyta się na głos np. “Czy ktoś czeka na nagrania?”, bo czasami ludzie czają się w okolicy i czekają na pytanie), to konwentowicz można nagrać kolejną rzecz (znowu na zasadzie 2-3 prób, po których następuje sprawdzenie okolicy).  
Zrobimy konfę informacyjną, gdzie opowiemy trochę o tym, jak rozmawiać z ludźmi.  
  
Gdy technik skończy pracę z konwentowiczem, daje znak zarządzającemu kolejką, by ogarniał kolejne osoby.

##### Reżyser
W naszej rozpisce reżyser to jeden z dwóch rozmawiających. To osoba, która przejmuje konwentowicza z punktu info i sprawia, że konwentowicz jest na każdym kroku zaopiekowany. Reżyser przedstawia się i przedstawia technika (np. "Cześć, jestem Pchełka. Tutaj przy komputerze jest Orzecho, będzie cię nagrywać"). Reżyser siedzi przy konwentowiczu i doradza mu, co poprawić w kolejnym podejściu, żeby było lepiej, ale także **kontroluje czas**. Czasami konwentowicze utykają w jakimś miejscu, a czas leci. Reżyser wtedy określa, które z dotychczasowych podejść było najlepsze i proponuje przejście do kolejnej kwestii. Nowicjusze lepiej reagują, gdy ktoś im coś podpowie lub nawet stwierdzi "okej, dalej". Po nagraniu reżyser odpowiada na krótkie pytania konwentowicza, a jeśli są dłuższe - odsyła do punktu info z hasłem "Pchełka ci wszystko chętnie opowie", żeby nie blokować kolejki.  

##### Technik
Nagrywający siedzą przy komputerze i ogarniają proces nagrywania. W tym roku jest tylko jedno stanowisko, więc trzeba dbać o to, by piosenki i scenki się nie mieszały. To technicy dbają o poprawne podpisywanie ścieżek.  
W piątek o 12.00 powinien być już rozstawiony cały sprzęt do nagrywania. Na komputerze stoiskowym zainstalowany zostanie Reaper, w którym będziecie zapisywać nagrania. 

  Nie będziemy sprzętu montować tak, by puszczać nagrania na głośnikach - za dużo przełączania, a i tak nie słychać, bo głośno. Nagraną scenkę/piosenkę prezentujemy nagrywającemu na słuchawkach.
* Piosenki wysyłamy jako surowe, nieobrobione nagrania samego głosu, w folderze zbiorczym na Facebooku; dołączymy podkłady, gdyby ktoś chciał to sobie zmiksować.
* Scenki wrzucamy na YT jak w poprzednich latach; nie wysyłamy pojedynczych nagrań prywatnie.

Pracujecie z konwentowiczami jeden na jeden, więc jeśli jakaś grupka chce nagrać, to każdy musi osobno, niech nie drą się wspólnie do jednego mikrofonu, bo potem burdel i to się nadaje tylko do kosza.  
Każde nagranie opisujecie PRZED nagraniem (bo po nagraniu ludzie uciekają, serio). Ścieżkę opiszcie nickiem, najlepiej wyjątkowym (kto by chciał 20 ścieżek “Ania”?), upewnijcie się, że został poprawnie zapisany, niech konwentowicz wam zerknie przez ramię. Dodatkowo jeśli ktoś chce potem być w scence z kumplem, to w nawiasie za jego nickiem musi być o tym info, np. "Pchełka (z Orzechem, NieQkim i NieKoczem)". Wtedy nawet po konwencie będzie wiadomo, by połączyć w scence odpowiednie osoby.

### Co będzie dostępne podczas Live Dubbingu?

##### Dwie scenki
Jedna z anime, druga z nie-anime. Koniecznie obejrzyjcie obie, żebyście wiedzieli, co polecać ludziom. Kierujcie ludźmi tak, by mieć nagrania na każdą rolę. Reżyserzy, pytajcie technika, czy zostały jeszcze jakieś nieobsadzone role!  
Plan jest taki, by po zakończeniu konwentu wydać wideo, w którym zamieścimy te scenki z głosami nagranymi podczas konwentu. Przykład:
<iframe width="560" height="315" src="https://www.youtube.com/embed/AKOW5k8vM70" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>  
  
Dlatego też, jeśli ktoś zgadza się, by zapisać jego nagrania na komputerze NK, zgadza się na publikację swojego głosu - to ważne, by ludzie mieli tego świadomość. Tutaj musicie też im zaznaczyć, że chociaż sprzęt jest dobry, to dźwięki tła robią swoje.  
Jedna osoba może nagrać się do tylu postaci, do ilu chce, ale po każdej postaci musi stanąć na nowo w kolejce!  
Scenki będą dostępne z poziomu pulpitu, każda w swoim folderze razem ze scenariuszem. W tym samym folderze znajdziecie plik reaperowy, który będzie mieć już wrzuconą scenkę. Zanim zaczniecie nagrywać, zróbcie nową ścieżkę w grupie danej postaci i ją nazwijcie nickiem konwentowicza. To ułatwi mi potem pracę, serio!  

##### Dwanaście piosenek
Muszą mieć do 2 minut, być dziecioprzyjazne i musimy mieć do nich podkład (pasujący timingowo do oryginału) i oryginał. Jeśli podrzucicie mi pliki (oryginał, podkład, tekst), to je dodam do listy. Jak nie dostanę nic nowego, biorę te z zeszłego roku.  
  
Dlaczego tylko tyle? Bo do nich będziemy mieć przygotowane projekty w reaperze: podkłady i oryginały. Doświadczenie lat mówi nam, że pobieranie podkładów i oryginałów na bieżąco jest upierdliwe, zajmuje dużo czasu, podkłady są nieprzycięte (full ver. vs TV ver.), wygląda to bardzo nieprofesjonalnie. Jeśli ktoś będzie chciał nagrać inną piosenkę niż zaproponowane, mówimy “Przykro mi, ale nie. Przygotowaliśmy się do nagrywania tych konkretnych piosenek” i tyle. Dla upierdliwców - zwalcie na siłę wyższą, czyli mnie.  
Tak czy siak, jak ktoś zdecyduje się na nagranie wokalu, musicie mu wtedy powiedzieć, że jakość będzie słaba, bo jesteśmy w hali, jest głośno etc.  Teksty NIE będą dostępne na kompie (bo nagrywający ma się zająć nagrywaniem, a nie scrollowaniem tekstu). Postaramy się mieć je wydrukowane, ale kartki lubią się gubić, więc w razie czego proście, by otwierali sobie teksty na smartfonach, będzie im i nam wygodniej.   
Nagrania opisujemy nickiem i po konwencie wysyłamy cały folder reaperowy, ludzie mogą sami pobrać całość i eksportować tak, jak chcą.
