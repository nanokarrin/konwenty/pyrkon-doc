---
id: ogolne
title: Poznań, 14-16 czerwca 2024
sidebar_label: Podstawowe informacje
---
## Gdzie?
Stoisko będzie na terenie festiwalu. Będzie podlegać pod Strefę M&A, w pawilonie 7. Będzie tuż od wejścia bramkowego od strony ulicy Śniadeckich. Będziemy pomiędzy dwoma innymi stoiskami. Dostaniemy parę krzeseł i ławki.   
NIE będziemy mieć miejsca na przechowywanie walizek czy rzeczy prywatnych jak w poprzednich latach. Strefa M&A wymaga, byśmy bagaż podręczny przechowywali w szatni, będzie specjalna wydzielona dla strefowiczów.  

## Godziny funkcjonowania stoiska

Piątek: 12.00-21.00  
Sobota: 10.00-21.00  
Niedziela: 10.00-16.00  

W piątek o 11.30 stoisko powinno być w pełni sprawne i rozłożone. Koordynatorzy będą mogli wejść już w czwartek po południu oraz w piątek rano.  
W niedzielę od 16.00 zaczynamy się zwijać.    
Na pewno będziemy potrzebowali pomocy w czwartek z rozłożeniem stoiska.    

## Koordynatorzy
Dostaniemy 14 wejściówek, osoby, które je dostaną, mają 10-12 godzin dyżuru minimum.   
Pchełka   
Orzecho   
Evil   
Funfel   
Hazu   
Hikari   
Marime   
inlaru   
Der0   
maseil   
Persephone   
SusieFiedler   
Qki   
Wilczyca   

Dodatkowo są tzw koordynatorzy, ludzie od zadań specjalnych, którzy mają jeszcze więcej roboty:   
Opiekun stoiska (zdalny), przygotowanie do konwentu: Pchełka  
Koordynator harmonogramu i sprzętu: Orzecho  
Koordynator wystroju stoiska: Marime  
Koordynator ścianki fotograficznej: Orzecho  
Koordynator mediów: Persephone  
Koordynator atrakcji: Maseil  


## Formularz i harmonogram
##### ➨➨➨ [Link do formularza zgłoszeniowego](https://forms.gle/nMSu5J8EyCDFQ9tj9)
Atrakcje stałe możemy zgłaszać do 24 marca, Panele 31 marca.   
Po tym czasie zgłoszenia też będą możliwe, ale już bez wejściówek. Niemniej, każda pomoc się przyda!   

##### ➨➨➨  [Link do harmonogramu](https://docs.google.com/spreadsheets/d/1qCwnBisBk5OUjVd4scXArOvEehvbGvC83oSYNjSlHuc/edit#gid=0&range=A1)
Dyżury dla organizacji mamy określić do 14 kwietnia.   
Pierwszą wersję do weryfikacji udostępnimy w marcu/kwietniu. Będziemy o tym informować na dedykowanym kanale discordowym. 
* UWAGA! Błagam, nie przywiązujcie się do pierwszej wersji. Zwykle komuś coś nie pasuje, coś trzeba przestawić, a zmiany idą lawinowo. Czyli jeśli zgłosicie dwa okienka i wybierzemy jedno z nich, może się okazać, że przy poprawkach jednak skorzystamy z drugiego. Aż do przyklepania przez nas harmonogramu zostawcie otwarte czasowo te opcje, które nam zapisaliście w formularzu.
* Jeśli kogoś nie lubicie i nie chcecie być z nim na dyżurze, napiszcie to.
* Jeśli chcecie być z kimś na dyżurze, to dopilnujcie, byście obydwoje wpisali dane godziny i byście uzupełniali swoje role :D _Zdarzało się, że osoby z niepokrywającymi się harmonogramami chciały mieć wspólny dyżur._

## Zdjęcie grupowe
TYM RAZEM zrobimy zdjęcie przy stoisku - będzie mniej biegania.   
SOBOTA, 12:30   

## Merch NK / Koszulki
Merchem w tym roku zajmuje się inlaru, zapraszamy na kanał #merch.  
Dyżury merchowe są określone w harmonogramie wyżej. 