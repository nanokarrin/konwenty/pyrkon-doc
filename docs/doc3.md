---
id: howtodyzur
title: Ogólne zasady dyżurów
sidebar_label: Ogólne zasady dyżurów
---

### Dbamy o bezpieczeństwo
Cały sprzęt na stoisku, tj. kable, rozgałęziacze, komputer, głośniki, mikrofon, statyw etc., należy do członków NK. Byłoby szkoda, gdyby ktoś go sobie “pożyczył”. Nie pozwalamy obcym korzystać z laptopa, nie pozwalamy podłączać nośników, np. pendrive’ów (kiedyś w ten sposób rozprowadzano wirusa), chyba że mamy do czynienia z koordynatorem lub właścicielem sprzętu.  
Nie zostawiajcie stoiska bez opieki. Harmonogram jest jawny, wiecie, kto jest przed wami i za wami. Przed konwentem koniecznie weźcie do siebie numery telefonów i się kontaktujcie, gdyby zaszła taka potrzeba. Gdy wszystko zawiedzie, dzwońcie do koordynatora. Siedzicie na stoisku, dopóki ktoś was nie zmieni, więc bądźcie na czas, żeby nie zabierać go innym.  
Do krzeseł, które będą przy stoisku, pierwszeństwo mają konwentowicze, którzy się nagrywają, członek NK, który ich nagrywa oraz członkowie NK siedzący na froncie. Jeśli akurat nie pełnicie dyżuru, ale chcecie posiedzieć, miejcie świadomość, że może zabraknąć dla was krzesła. Nie siadajcie wtedy na stole, idźcie ukraść krzesło skądinąd.  


### Zarządzamy przepływem ludzi
Czas, gdy zmienia się dyżur, to moment chaosu. Nie bójcie się wtedy zarządzić kilkuminutowej przerwy technicznej. Podczas przerwy dwie osoby z nowych dyżurujących ustawiają pod siebie sprzęt, trzecia  zajmuje gości stoiska, prosi o chwilę cierpliwości i tłumaczy, co się będzie działo za chwilę, zachęca do skorzystania z innych atrakcji stoiska. Dopiero kiedy nowi techniczni siedzą wygodnie i wiedzą, co się dzieje na kompach, prosimy konwentowiczów do nagrywania.  

__Uwaga!__ Nigdy nie przerywamy nagrań z osobą, która już zaczęła, tzn. nie mówimy jej “dobra, stop, mamy przerwę, wróć za chwilę”, tylko kończymy nagrywanie na spokojnie i dopiero wtedy robimy przerwę.  


### Opowiadamy o działalności NK
Mówimy o tym, co to jest NanoKarrin, czym się zajmujemy, gdzie można nas znaleźć. Zainteresowanym opowiadamy o możliwych do zdobycia rangach i o rekrutacji. Wymagania pokrótce:  
- aktor: nagranie “Krasnoludków” Brzechwy oraz zdubbingowanie w scence postaci    
- dialogista: stworzenie dialogów do scenki  
- dźwiękowiec: stworzenie ścieżki dźwiękowej do wybranej przez siebie scenki
- tekściarz: przetłumaczenie meliczne tekstów dwóch piosenek, każda o długości  min. 1:30
- realizator: zmiksowanie jednej piosenki
- wokalista: zaśpiewanie dwóch piosenek, o długości min. 1:30
- ilustrator: zaprezentowanie projektów graficznych, styl dowolny
- animator: zaprezentowanie filmów do piosenek, każdy o długości min. 1:30

Będziemy mieć także do rozdania wizytówki, ale jako że jesteśmy biedni, będzie ich mało, więc bez szaleństw.