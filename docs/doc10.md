---
id: poprzedni
title: Wasze wnioski 2019, 2022, 2023
sidebar_label: Wasze wnioski 2019, 2022 i 2023
---

### Wybrane cytaty z anonimowej ankiety 2023
:::important
Wklejam niektóre! Było sporo pozytywów, ale daję te, które mogą nas pobudzić do działania. A. I ja nie za bardzo mam czas przyłożyć rękę do wszystkiego, więc jeśli kogoś poruszy któraś sprawa / któryś problem i będzie chciał zająć się rozwiązniem, niech się do mnie zgłosi.
::: important

> Losowanie w Bingo, jak mój znajomy stwierdził (z czym się zgadzam), powinny być w pudełku, a nie w siatce, wtedy są w pewnym stopniu wyrównane szanse dla tych, którzy wcześniej oddali kartę

> Wydawało mi się, że w tym roku ludzie na stoisku byli bardziej proaktywni, super było to widzieć! W przyszłym roku pewnie lepiej dogadać kwestie sprzętu i sprzątania z większym wyprzedzeniem. 

> Podobało mi się też, jak kiedyś mieliśmy wydruki artów Karrin zawieszone na stoisku, nie tylko kolorowanki randomów. Może warto do tego wrócić? I kupić lepszą taśmę klejącą.

> Mapa NK - koniecznie wydrukować, ale zrobić malutkie najklejki z okrągłym logo do zaznaczania

> Zrobić rollup z info, kim jesteśmy i co robimy, bo napis głosi NK i nie wiadomo. może nawet specjalny rollup pyrkonowy?

> Fajny był papierowy obrus na stół, tylko można by dawać jeden dziennie, bo się gniótł. Do tego jakiś ilustrator mógłby dać duży rysunek markerem od razu, żeby bylo widać, ze można rysować.

> Dwie kopie scenariusza powinny leżeć przy stanowisku podpisane, że są stanowiskowe - bo na tym pyrkonie kradli/jedli scenariusze i Marime biegała dodrukowywać.

> 100 arkuszy bingo było za mało, poszło 150, a mogłoby być spokojnie 200.

> Nagrody Bingo tylko dla osób, które się zjawią na losowaniu. Nie dzwonimy, nie wysyłamy, nie mailem. bo głupio wobec tych, którzy się pofatygują.

> Osoba na froncie, jeśli rozmawia, nie ma czasu pilnować kolejki. Trzeba jedną, osobną osobę od kolejki.

> Koncert - trzeba pilnować wokalistów z naklejkami od backstage'u, żeby nie odbierali na ostatnią chwilę

> Ludzie do końca nie ogarniali, co mają zrobć. Może tuż przed konwentem wyslać im quiz do zrobienia? Żeby musieli zastanowić się nad odpowiedziami.

> Ogólnie stoisko bardzo fajne, ale dobrze by było urozmaicić ofertę dialogów do odegrania oraz zrobić trochę więcej punktów programu. O i w sumie można byłoby robić "live acting" - zbiera sie grupa ludzi (i członkowie NK i "normalsi" :P) i na żywo bez nagrywania można robić scenki/pośpiewać itp. żeby była taka integracja między członkami NK a ludźmi zainteresowanymi. Można też zrobić konkurs na dokończenie piosenek (a'la "Dziekie Ucho" :P). Tak to wszystko gra.

> Było super, tylko fajnie by było gdyby były kahooty dotyczące tego, którą linijka tekstu pojawiła się w tym momencie w filmie albo jakieś koła fortuny

> Ogarnianie kącika rysowniczego - są osoby które mimo wszystko sprzątały fajnie i starały się zachować porządek w swoich godzinach aczkolwiek były godziny w których nikogo nie było w kąciku a sam kącik wyglądał jak 7 nieszczęść i trzeba było go szybko ogarnąć

> Dyżury niektórych osób i dobranie/wyszkolenie niektórych osób - były osoby które na swoim dyżurze niezbyt zajmowały się tym co miały np odchodziły od stoiska, rysowały sobie w kąciku czy też rozmawiały ze znajomymi nie na temat NK ani stoiska. Myślę że też obstawienie osób na sam koniec było meh bo były dwie osoby które ledwo ogarniały bo nie był nikt inny wpisany, ludzie uznali że nie muszą albo chcieli coś zrobić skoro mają przerwę (i do tego pełne prawo mieli), a osoby na stoisku nie mogły nawet pójść się załatwić bo stoisko zawalone ludźmi i nie ma kto przejąć

> router na stoisko?

> https://mojekrowki.pl/cennik/ może krówki za nagrania?


### Wybrane cytaty z anonimowej ankiety 2022

> "dajcie marker od razu przypisany do mapy"

> "długopisów do list czasem brakowało"

> "Co myślicie o zrobieniu tabliczki na patyku do wołania nowej osoby w kolejce? Albo może dzwonek?"

> "Zróbcie widoczne ogłoszenie na stoisku rysowniczym, żeby skończone obrazki przyczepiać do ścianki"

> "więcej osób do nagabywania jest potrzebne albo jedna osoba do wychodzenia do ludzi, a jedna do pilnowania listy"

> "Może mapka w bardziej widocznym miejscu? Żeby ludzie nie zapominali!"

> "przydałaby się instrukcja jak włączyć sprzęt - kopiowanie ekranów etc., najlepiej przed Pyrkonem"

> "zrobicie techniczną listę otwierania stoiska i zdawania stoiska?"

> "Problemem, moim zdaniem, było zgromadzenie nadmiernej ilości osób z grupy wokół stoiska w sposób uniemożliwiający ruch... lub po prostu zawstydzający osoby spoza grupy do podejścia”

> "Brakowało statywu do mikrofonu dla osoby obsługującej stanowisko przez co musiała go trzymać sama, co przy dużej ilości klikania marnowało dużo czasu”

> "Można stworzyc księgę nk do ktorej mogłaby sie wpisac każda osoba z nk będąca na danym konwencie”

> "Kupcie stojaczek na wizytówki!”

> "trzeba po prostu jako must have mieć emilka na stoisku, bo gadał jak zawodowa nagabywaczka"

> "Dodatkowo w miarę możliwości zapewnić dwa stanowiska do nagrywania osobno dla wokali i scenek."

> "Można też spróbować wzbogacić ofertę warsztatową o jakieś warsztaty z dykcji, wokalu, pisania scenariuszy, nawet samego nagrywania oraz wiedzy o sprzęcie"

> "Bingo NK!"