---
id: jakuczestniczyc
title: Jak mogę uczestniczyć?
sidebar_label: Jak mogę uczestniczyć?
---

Jest kilka stref, w których możesz wesprzeć Pyrkonową działalność NK.

#### Przed Pyrkonem
* Jak w poprzednich latach, przygotowujemy dwie scenki (SFW!). Jedną z anime - Pamiętnik Zielarki, drugą z nie-anime - Secret Life of Pets 2.     
Potrzebuję dźwiękowców do: wycięcia fragmentu, przygotowania ścieżki dźwiękowej (nie musi być cud świata), eksportu pliku mp3 z foleyem.   


* Będą też do nagrania piosenki. Będzie ich 12. Jeśli macie propozycje (max. 2 minuty), podrzućcie mi proszę na PW oryginał, podkład oraz tekst.   
* Będziemy też robić zrzutkę $$$ na wystrój oraz wyposażenie stoiska (np. plakaty, wizytówki), żeby nie było takie gołe jak w 2019. Info o tym pojawi się na kanale z ogłoszeniami, jak będziemy mieć oszacowane koszta.  

#### Live Dubbing  
Możesz zapisać się na dyżur na stoisku, określając wcześniej, czym chcesz się zajmować bardziej:
- nagabywać ludzi, udzielać informacji o NK/stoisku, zapisywać do kolejki
- siedzieć przy komputerze i być technicznym, który pilnuje porządku w plikach, nagrywa, dba o komfort nagraniowy konwentowicza  
Każdy, kto wpisze się na dyżur, dostanie dodatkowy minidyżur porządkowy (pozbieranie pustych puszek czy butelek, uporządkowanie obrazków w kąciku rysowniczym, patrz: Prawo i Niesprawiedliwość).

#### Fotościanka
Jak rok temu stawiamy fotościankę. Jeśli ktoś jest zainteresowany opieką/dyżurem fotograficznym lub dostarczeniem sprzętu, niech da znać.  

#### Tańcz z NK
Minieventy, gdzie nagrywamy tiktoki do naszych wersji piosenek.   

#### Quiz dubbingowy
Minievent, gdzie przepytujemy ludzi z dubbingów.

#### Koncerty
W tym roku nie ma możliwości koncertowania.

#### Sprzęt i przydasie
Potrzebujemy ludzi, którzy dostarczą na konwent sprzęt na live dubbing (monitory!, kable, mikrofony, statywy, etc.). Bardzo nie chcę tego wozić z Wwy, bo to upierdliwe, więc liczę na Poznań i okolice.  
Dodatkowo będziemy potrzebować kredek/markerów/innych rzeczy do rysowania do Kącika Rysowniczego, nożyczek, taśmy klejącej do wieszania, długopisów, czystych kartek, worków na śmieci, foliopisów do plakatów... Jeśli możecie wziąć choć jedną z tych rzeczy - super! Bierzcie i przynieście na stoisko!   

#### Media
Persephone zbiera drużynę, która poprowadzi grupę do sukcesu. Fajnie byłoby mieć z Pyrkonu i filmiki, i zdjęcia, i relację na insta, i coś na fb. Jedna osoba tego nie ogarnie, a jeśli ktoś tak czy siak lubi robienie fotek, to nich przemyśli robienie fotek dla NK. Nie musi to być zdjęcie stoiska jako takiego, ale też nas, ludzi, jak się dobrze bawimy, tulimy etc.  

#### Po Pyrkonie
Będzie dużo pracy. Wciąż.  
* Potrzebuję dzielnych śmiałków, rangi mnie nie obchodzą (sama nie mam), którzy zmontują dostarczone materiały (głosy aktorów z ambientem aka ścieżką dźwiękową). Będę o tym jeszcze pisać, ale im więcej osób się zgłosi, tym mniej scenek na osobę! To nie jest wybitne trudne, nie wymagam idealnego czyszczenia, wystarczy Reaper.  
