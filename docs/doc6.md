---
id: warsztaty
title: Atrakcje dodatkowe
sidebar_label: Atrakcje dodatkowe
---


### Panele
W tym roku zaakceptowany panel to panel maseil o tłumaczeniu piosenek. Jest wyszczególniony w harmonogramie i w Konwenciku.


### Warsztaty
Na stoisku możemy poprowadzić miniatrakcje, np. warsztaty, ale będą iść równolegle z nagrywaniem Live Dubbingu. Robimy warsztaty aktorskie. 

### Bingo!
Rok temu zrobiliśmy bingo na stoisku, okazało się sukcesem. W tym roku powtórzymy sprawę. Potrzebuję więcej zadań i pomysłów! Póki co mamy to:  

https://osric.com/bingo-card-generator/?title=NanoKarrin+x+Pyrkon+2024%21&words=Czy+NK+to+jedna+osoba%3F%2CZr%C3%B3b+gwiazd%C4%99%2CZr%C3%B3b+pi%C4%99%C4%87+pajacyk%C3%B3w%2CZr%C3%B3b+zdj%C4%99cie+przy+naszej+foto%C5%9Bciance%2CWrzu%C4%87+relacj%C4%99+przed+naszym+stoiskiem+i+oznacz+NK%2CZr%C3%B3b+selfie+z+dowoln%C4%85+Karrin%2COdpocznij+na+le%C5%BCaku+NK%2CZa%C5%9Bpiewaj+zwrotk%C4%99+Pyrkonowej+Pie%C5%9Bni%2CPowiedz+suchara%2CPokoloruj+Karrin+w+K%C4%85ciku+Rysowniczym%2CW+kt%C3%B3rym+roku+powsta%C5%82o+NK%3F%2CKto+jest+Kr%C3%B3lem+NanoKarrin%3F%2CCzym+zajmuje+si%C4%99+NanoKarrin%3F%2CCo+to+s%C4%85+Bitwy+Dubbingowe%3F%2CCzym+zajmuje+si%C4%99+D%C5%BAwi%C4%99kowiec+NK%3F%2CCzym+zajmuje+si%C4%99+Animator+NK%3F%2CPrzywitaj+si%C4%99+na+serwerze+Discord+NK%2CZasubskrybuj+Youtube+NK%2CZr%C3%B3b+fotk%C4%99+z+map%C4%85+Pyrkonu%2CPrzyprowad%C5%BA+na+stoisko+NK+znajomego%2CNagraj+si%C4%99+do+scenki+na+stoisku%2CZa%C5%9Bpiewaj+%E2%80%9CGrosza+daj+wied%C5%BAminowi%E2%80%9D%2CDaj+nasz%C4%85+wizyt%C3%B3wk%C4%99+komu%C5%9B+spoza+hali+i+zapro%C5%9B+go+na+nasze+stoisko%2CJakie+zwierz%C4%99+jest+symbolem+NK%3F%2CKto+odpowiada+w+NK+za+konwenty%3F%2CJaki+jest+nasz+najnowszy+projekt+na+YT%3F%2CZr%C3%B3b+zdj%C4%99cie+przy+naszym+rollupie%2COznacz+si%C4%99+na+Dubbingowej+Mapie+Polski%2CZaklaszcz+Rubika%2CWymie%C5%84+trzy+bajki+w+kt%C3%B3rych+podoba%C5%82+Ci+si%C4%99+dubbing&freespace=false&freespaceValue=&freespaceSubheadingValue=Free+Space&freespaceRandom=false&width=5&height=5&number=50#results

Jako nagrodę zasponsoruję kubki NK.  