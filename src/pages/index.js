import React from 'react';
import classnames from 'classnames';
import Layout from '@theme/Layout';
import Link from '@docusaurus/Link';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import useBaseUrl from '@docusaurus/useBaseUrl';
import styles from './styles.module.css';

const features = [
  {
    title: <>Live Dubbing</>,
    imageUrl: 'img/wolne-dubbingowanie.jpg',
    description: (
      <>
        Nasza najstarsza i dla wielu główna atrakcja. Mobilne studio nagrań, w którym każdy uczestnik konwentu może spróbować swoich sił, siadając przed mikrofonem.
      </>
    ),
  },
  {
    title: <>Kącik rysowniczy</>,
    imageUrl: 'img/kacik.jpg',
    description: (
      <>
        Nasza społeczność przyciąga i skupia wiele osób utalentowanych plastycznie. Ten fragment stoiska jest własnie dla nich.
      </>
    ),
  },
  {
    title: <>Koncerty</>,
    imageUrl: 'img/piko.jpg',
    description: (
      <>
        Prezentacja umiejętności naszych wokalistów.
      </>
    ),
  },
];

function Feature({imageUrl, title, description}) {
  const imgUrl = useBaseUrl(imageUrl);
  return (
    <div className={classnames('col col--4', styles.feature)}>
      {imgUrl && (
        <div className="text--center">
          <img className={styles.featureImage} src={imgUrl} alt={title} />
        </div>
      )}
      <h3>{title}</h3>
      <p>{description}</p>
    </div>
  );
}

function Home() {
  const context = useDocusaurusContext();
  const {siteConfig = {}} = context;
  return (
    <Layout
      title={`${siteConfig.title}`}
      description="NanoKarrin na Pyrkonie 2023">
      <header className={classnames('hero hero--primary', styles.heroBanner)}>
        <div className="container">
          <h1 className="hero__title">{siteConfig.title}</h1>
          <p className="hero__subtitle">{siteConfig.tagline}</p>
          <div className={styles.buttons}>
            <Link
              className={classnames(
                'button button--outline button--secondary button--lg',
                styles.getStarted,
              )}
              to={useBaseUrl('docs/ogolne')}>
              Dokumentacja
            </Link>
          </div>
        </div>
      </header>
      <main>
        {features && features.length && (
          <section className={styles.features}>
            <div className="container">
              <div className="row">
                {features.map((props, idx) => (
                  <Feature key={idx} {...props} />
                ))}
              </div>
            </div>
          </section>
        )}
      </main>
    </Layout>
  );
}

export default Home;
